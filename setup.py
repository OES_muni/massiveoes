##package for batch processing of optical emission spectra
##Authors: Jan Vorac and Petr Synek
##contact: vorac@mail.muni.cz, synek@physics.muni.cz

from setuptools import setup, find_packages
install_requires = ['lmfit>=0.9.13' ,
                    'scipy>=1.3.0',
                    'numpy>=1.16.4',
                    'asteval>=0.9.14',
                    'pandas>=0.24.2',
                    'jupyter==1.0.0',
                    'ipywidgets==7.4.2',
                    'plotly==4.6.0'
                    ]


# uncomment if you wish to use the deprecated GUI
# install_requires.extend(['PyQt5', 'pyzmq', 'enable', 'traits', 'traitsui', 'chaco'])

setup(
    name = "massiveOES",
    version = "1.001",
    packages = find_packages(),
    install_requires=install_requires
)
