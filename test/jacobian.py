from massiveOES import LargeSimulatedSpectra, MeasuredSpectra, puke_spectrum
import lmfit
import numdifftools as nd
import numpy as np


sim_OH = LargeSimulatedSpectra.load('/home/janvorac/measurements/2014-07-29_surfatronOES/simulations/OH_noslitfc.pkl')

meas = MeasuredSpectra.load('/home/janvorac/measurements/2014-07-29_surfatronOES/smazme.meas')
#meas = MeasuredSpectra.from_FHRfile('/home/janvorac/measurements/2014-06-24_k_svatku/Ar/Profil_80Hz/OH_310/ICCD_ar_profil_80Hz_1.txt', [sim_OH])

specname = meas.spectra.keys()[0]

print(meas.spectra[specname]['params']['wav_step'])find and

meas.spectra[specname]['params']['wav_2nd'].value = -1e-7
meas.spectra[specname]['params']['wav_step'].value = 0.00965

# meas.spectra[specname]['params']['wav_start'].min = 306
# meas.spectra[specname]['params']['wav_start'].max = 308

meas.spectra[specname]['params']['slitf_gauss'].value = 5e-3
meas.spectra[specname]['params']['slitf_lorentz'].value = 5e-3
meas.spectra[specname]['params']['OH_Tvib'].vary = False


#meas.spectra[specname]['params']['OH_Trot'].value = 5e3

meas.fit(index, by_peaks = False, method = 'leastsq')


print meas.minimizer.message

print meas.minimizer.lmdif_message

lmfit.report_fit(meas.minimizer.params)

# import pickle
# filename = 'OH_params.list'
# with open(filename, 'wb') as output:
#     pickle.dump(meas.parameters_list, output, pickle.HIGHEST_PROTOCOL)

#meas.minimizer._Minimizer__residual(meas.minimizer.params)


# meas.spectra[specname]['params']['wav_start'].vary = False
# meas.spectra[specname]['params']['wav_step'].vary = False
# meas.spectra[specname]['params']['wav_2nd'].vary = False

# #meas.minimizer.prepare_fit(meas.spectra[specname]['params'])

# #print meas.spectra[specname]['params'].prms

# meas.fit(index, by_peaks = False, method='leastsq')
# lmfit.report_fit(meas.minimizer.params)

#print 'vars = ', meas.minimizer.vars


# step_noms = []
# for var in meas.minimizer.vars:
#     step_noms.append(var*5e-3)

# if 'wav_start' in meas.minimizer.var_map:
#     step_noms[meas.minimizer.var_map.index('wav_start')] = 1e-2
# if 'wav_step' in meas.minimizer.var_map:
#     step_noms[meas.minimizer.var_map.index('wav_step')] = 1e-5


# Dfun = nd.Gradient(meas.minimizer.penalty, step_nom = step_noms)
# Dfun.verbose = True 
# d = Dfun(meas.minimizer.vars)

# print d

#zamezit prilisnym zmenam parametru wav_ !!!!!!!!!!!!!

# Jfun = nd.Jacobian(meas.minimizer._Minimizer__residual)#,step_nom=step_noms)
# Jfun.verbose=True

# jac = Jfun(meas.minimizer.vars)

# print 'jac = '
# print jac

# Hfun = nd.Hessian(meas.minimizer.penalty)

# hess = Hfun(meas.minimizer.vars)
# print 'hess = '
# print hess


# from numpy.linalg import inv
# def get_covariance_matrix(hess):
#     return inv(hess)

# def get_corr_matrix(cov):
#     corr = np.zeros((cov.shape))
#     for param1 in range(cov.shape[0]):
#         for param2 in range(cov.shape[1]):
#             corr[param1, param2] = cov[param1, param2]/np.sqrt(cov[param1, param1] * cov[param2, param2])
#     return corr

# dof = len(meas.minimizer.residual) - len(meas.minimizer.vars)
# susmq = np.sum(meas.minimizer.residual**2)

# def get_uncertainties(cov, sumsq, dof):
#     ret = np.sqrt(sumsq/dof * abs(np.diagonal(cov)))
#     return ret


# hess2 = np.dot(jac.T, jac)

# cov2 = get_covariance_matrix(hess2)

# corr2 = get_corr_matrix(cov2)
# unc = get_uncertainties(cov2, susmq, dof)

# a = np.array(meas.minimizer.vars)

# print unc/a
# print meas.minimizer.var_map
