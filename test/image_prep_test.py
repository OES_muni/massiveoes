import massiveOES, piUtils, numpy
import os

p = '/home/janvorac/measurements/2017-03-07-OES_DBD_atomizator/'
path1 = p+'336p8nm_100sccm_Ar_15_sccm_H2_wet_25V_1A_dark_ver-center_hor_0top-18bottom_mm.spe'
path2 = p+'336p8nm_100sccm_Ar_15_sccm_H2_wet_25V_1A_dark_ver-center_hor_0top-18bottom_mm_dark.spe'
division_size = 5 # pocet pixelu pro uvodni deleni na hledani bendu
treshold = 1000 # celkove maximum

image = piUtils.readSpe(path1)
dark =  piUtils.readSpe(path2)

accumulations = int(image['footer'].getElementsByTagName('Accumulations')[0].firstChild.nodeValue)
accumulations_dark = int(dark['footer'].getElementsByTagName('Accumulations')[0].firstChild.nodeValue)

if accumulations != accumulations_dark:
    print(path1, path2, ' Accumulations mismatch!')

throwaway,codename = os.path.split(path1)

#2D obrazky z kinetickych serii
dark  = numpy.mean(numpy.array(dark['data']), axis=0)
image = numpy.mean(numpy.array(image['data']), axis=0)

image_preparator = massiveOES.Preparator(codename, image, dark)

image_preparator.preprocess_dark()
print('Dark substracted.')

image_preparator.preprocess_wavs()
print('Open file '+codename+'_initial_wavs' + '.json'+' and process it than press any key.')
null = input("W8 for key pressed...")
assert image_preparator.preprocess_wavs()

image_preparator.divide_et_impera(division_size)
print('Open file '+codename+'_divided_wavs' + '.json'+' and process it than press any key.')
null = input("W8 for key pressed...")
image_preparator.find_the_bend()
image_preparator.final_cut(treshold)
