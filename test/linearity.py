from massiveOES import SpecDB#, SimulatedSpectra
import numpy as np
import matplotlib.pyplot as plt

n2DB = SpecDB('../massiveOES/data/N2CB.db')
specs = []
trots = []
Trot = 1000
for Tvib in range(300, 10000, 250):
    trots.append(Tvib)
    specs.append(n2DB.get_spectrum(Trot, Tvib))

tpl1 = []
tpl2 = []
tpl3 = []

wl1 = specs[0].x[5]
wl2 = specs[0].x[10]
wl3 = specs[0].x[50]
for spec in specs:
    tpl1.append(spec.y[spec.x==wl1])
    tpl2.append(spec.y[spec.x==wl2])
    tpl3.append(spec.y[spec.x==wl3])

plt.plot(trots, tpl1, 'o-')
plt.plot(trots, tpl2, 'o-')
plt.plot(trots, tpl3, 'o-')
plt.show()
